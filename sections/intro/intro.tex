

\section{Introduction}
Consensus Protocols have been an active area of research since the birth of Computer Science as a field of study. Consensus between participants in a distributed system is reached when they all agree on a single, correct value proposed by one the participants. A \textbf{consensus protocol} must exhibit the following properties:

\begin{enumerate}
    \item \textbf{Termination}: All participants must eventually decide on a value.
    \item \textbf{Validity}: The decided value must be correct (i.e. it must have been proposed by a valid participant).
    \item \textbf{Agreement}: Every process must decide on the same final value. 
\end{enumerate}

It has been proven that consensus in a completely asynchronous network with even a single failure is impossible~\cite{Fischer:1985:IDC:3149.214121}, but many consensus protocols on semi-sychronous networks have been proposed and are used in real-world systems. In this paper, we focus on consensus protocols tailored for blockchain networks. These protocols must exhibit, in addition to the properties above, tolerance against \textit{adverserial} participants in the network. 

\subsection{Byzantine Generals}
The Byzantine Generals Problem~\cite{lamport1982the}, introduced in 1982, describes the most famous scenario for reasoning about consensus. Imagine more than two generals surround an enemy city with their armies. Their goal is to conquer the city and in order to be successful, they must all attack at the same time. In other words, the generals must all \textit{agree} on an attack time. Now suppose that some of the generals are traitors. A malicious general does everything possible to prevent agreement between loyal generals. For example, they might pass on a fake message about another general's proposed time to attack. \cite{lamport1982the} defines the problem: 


\vspace{10px}
\noindent\fbox{%
    \parbox{\linewidth}{%

\textbf{\textit{Byzantine Generals Problem.}} A commanding general must send an order to his $n-1$ lieutenant generals such that 

\textbf{IC1.} All loyal lieutenants obey the same order. \\ 
\textbf{IC2.} If the commanding general is loyal, then every loyal lieutenant obeys the order he sends.

    }%
}
\vspace{10px}

In general, a \textbf{Byzantine Fault} occurs whenever a participant in a distributed system behaves maliciously or otherwise inappropriately. A system is \textbf{Byzantine Fault Tolerant} (BFT) if it behaves correctly in the face of these kinds of failures. Byzantine Fault Tolerance is difficult to achieve because adversaries are permitted to do \textit{anything} and \textit{everything} they can to prevent nominal behavior. In practice, distributed consensus protocols are generally BFT up to a certain proportion of adversaries or under additional contraints.

\subsection{Blockchain}
In Blockchain networks, adversaries try to commit invalid transactions to the ledger. Byzantine Fault Tolerant consensus protocols are necessary to protect the validity of the public ledger. A primary goal of this paper is to compare and contrast the conditions under which different consensus algorithms are BFT.

